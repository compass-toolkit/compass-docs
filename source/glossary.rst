Glossary
========

.. meta::
   :description lang=en: This is a glossary of terms used across Compass.

.. glossary::

  Secure Sockets Layer
     A protocol to secure communication between networked computers.