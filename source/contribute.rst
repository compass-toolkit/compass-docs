Contribute
==========

.. meta::
   :description lang=en: Find out how you can contribute to the Compass Toolkit.

Developing tools to be deployed on Compass is fairly straightforward. Developers have the freedom to select any technology stack that allows them to deploy their tool as a cloud-native web application. This allows each tool to have independent code repositories and development pipelines and contribution policies (such as being open-source).

Because we use Docker as our container technology, we, therefore, require tools to be containerized using Docker to enable us to deploy and manage the application using Compass. The image for the containerized application needs to be stored in a container registry accessible to Compass which takes care of deploying and managing the access and availability of the tool. Tools deployed on Compass will be showcased on the Explore page as shown below.

.. image:: ../images/compass_page.png
  :alt: A screenshot of the Compass Explore page.

If you are interested in proposing a new tool or modification to an existing tool at Compass, please `get in touch with us <https://carleton.ca/jaskolka/supervision/>`_.