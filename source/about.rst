About
=====

.. meta::
   :description lang=en: Read about the story behind Compass.

System architects, developers, and evaluators need simple and intuitive tools to help them design secure software systems. While a number of secure system design tools exist, it is hard for system architects and other stakeholders to keep up with the availability and improvement of these tools over time. This problem is compounded as more secure system design tools are released.

To tackle these challenges, we developed Compass, a web-based toolkit to house secure system design tools. The goal with Compass is to create a one-stop-shop for secure system design tools to help system architects and other stakeholders keep up with the different tools and techniques to design secure systems.

With Compass, we also provide design and usability guidelines to help researchers and develop tools that are intuitive for system architects to use. We believe this toolkit will inspire more secure system design researchers to develop tools for practitioners to apply, try out, and improve. This aims to address the lack of adequate tool support for practitioners to apply secure system design research in their workflows.

.. note::
 Compass toolkit is discussed in greater depth in [CARLETON2021]_ and [QRS2021]_.