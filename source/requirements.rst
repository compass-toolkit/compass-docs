Requirements
============

.. meta::
   :description lang=en: Learn about the requirements to develop a Compass tool.

With Compass, we aim to abstract the challenges associated with deploying and maintaining web-based secure system design tools. Such challenges include scaling tools based on usage, handling security-related concerns such as establishing secure connections between the tools and users, managing the availability of tools, and more. By abstracting these concerns, we seek to enable researchers and developers to primarily focus their efforts on the functionality and intuitiveness of their tools rather than the logistics of deploying a web-based application.

However, we do have a few requirements for a tool to be made available through Compass:

- Solve a security-related problem
- Be web-based (API, GUI, or Both)
- Be cloud-native (containerized using Docker)