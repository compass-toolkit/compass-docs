Build
=====

.. meta::
   :description lang=en: Learn how to build a tool for the Compass Toolkit.

Developing tools to be deployed on Compass is fairly straightforward. Developers have the freedom to select any technology stack that allows them to deploy their tool as a cloud-native web application. This allows each tool to have independent code repositories and development pipelines and contribution policies (such as being open-source). This versatility is achieved by the requirement for your application to be containerized for deployment.

Containerize your application
-----------------------------

Containerization refers to the packaging of a software application's code along with an operating system and associated dependencies into a lightweight executable called a container [IEEE2014]_. Containers provide isolation from the host environment and enable the application to perform in a predictable manner since it was developed and deployed within the same [container] environment. Containers help in standardizing different types of applications to run on any supported infrastructure (one that can run that particular container technology). Containers are saved as container images and those images are stored in container registries. To instantiate the application, the container image needs to be pulled from the container registry and the appropriate container technology can be used to create one or more containers (instances of the application) based on the container image. This allows an application to be replicated, instantiated, and managed independently which helps in maintaining high availability and resiliency. For Compass, we use Docker as the container technology to create and deploy containers [SIGOPS2015]_. We chose Docker as it is a popular container technology that is commonly used to develop cloud-native applications. We illustrate the Docker containers associated with the different tools in Compass as blue rectangles (with the Docker logo) in the figure below.

.. image:: ../images/compass_infrastructure.png
  :alt: High-level view of Compass Infrastructure

Because we use Docker as our container technology, we, therefore, require tools to be containerized using Docker to enable us to deploy and manage the application using Compass. The image for the containerized application needs to be stored in a container registry accessible to Compass which takes care of deploying and managing the access and availability of the tool. Tools deployed on Compass will be shown on the Compass website as shown below.

.. image:: ../images/compass_page.png
  :alt: A screenshot of the Compass Explore page.

Containerizing your application using Docker starts with a Dockerfile. A dockerfile encapsulates the various requirements and sequence of steps necessary to get your application running. Dockerfile varies based on the application stack and you will be able to find Dockerfile templates for your technology stack. To learn more about creating a Dockerfile for your application, refer to `Best Practices for Dockerfiles <https://docs.docker.com/develop/develop-images/dockerfile_best-practices/>`_.

.. image:: ../images/docker.png
  :alt: Containerization Process

Once you configure a Dockerfile, you can run the application locally through Docker to ensure the application works as intended.


Setting up container registry
-----------------------------

Now that you have a working [Docker] container specification (aka. Dockerfile), we can generate a container image based on the Dockerfile. This container image will be used to generate one or more instances (docker containers) of your application. We therefore need to host this container image somewhere accessible so Compass can download and deploy your container image(s) as one or more application instances.

.. note::
 `Gitlab <https://gitlab.com/compass-toolkit>`_, the preferred code hosting platform for Compass Toolkit, provides a free container registry where your application container image can be hosted.


Setting up CI/CD
----------------

We can automate the process of generating and storing container image(s) of your application in the container registry using continous integration and deployment (CI/CD). CI/CD provides the ability to automate the process of building, running, and testing your application in its containerized form. To learn about how you can setup a CI/CD pipeline for your application, refer to `Best practices for using Docker Hub for CI/CD <https://docs.docker.com/ci-cd/best-practices/>`_.

Documentation
-------------

Documentation serves as a great reference for users to understand and use your tool effectively. There are many methods to offer your documentation. We recommend making your documentation available in the following url: <your_tool_url>/docs. This url format aligns your tool's access to documentation with other Compass tools.

.. note::
 For open-source tools, we recommend `readthedocs.io <https://readthedocs.io>`_ as a method to offer your documentation in a versatile, version-controlled manner where the documentation evolves with your code.