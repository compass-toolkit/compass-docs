References
==========

.. meta::
   :description lang=en: References related to the Compass toolkit.

.. [CARLETON2021] Samuel, J. F. (2021). A Data-Driven Approach to Evaluate the Security of System Designs (Doctoral dissertation, Carleton University).

.. [IEEE2014] Dua, R., Raja, A. R., & Kakadia, D. (2014, March). Virtualization vs containerization to support paas. In 2014 IEEE International Conference on Cloud Engineering (pp. 610-614). IEEE.

.. [SIGOPS2015] Boettiger, C. (2015). An introduction to Docker for reproducible research. ACM SIGOPS Operating Systems Review, 49(1), 71-79.

.. [QRS2021] Samuel, J., Jaskolka, J., & Yee, G. O. (2021, December). Analyzing Structural Security Posture to Evaluate System Design Decisions. In 2021 IEEE 21st International Conference on Software Quality, Reliability and Security (QRS) (pp. 8-17). IEEE.