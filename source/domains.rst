Domains
=======

.. meta::
   :description lang=en: Learn how to procure a domain for your Compass tool.

We offer custom subdomains for Compass tools under compass.carleton.ca. The format of subdomains will be *<tool_name>.compass.carleton.ca*. The subdomain will be generated and maintained by Carleton University's Information Technology Services (ITS). A custom Secure Sockets Layer (SSL) certificate will also be generated for the tool as part of the Compass infrastructure.